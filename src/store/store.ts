import {configureStore, ThunkAction, Action, combineReducers} from '@reduxjs/toolkit';
import {formReducer} from "src/react-redux-hook-form-v2/src";

const combinedReducer = combineReducers({
  formStore: formReducer,
});

const rootReducer = (state:any, action:any) => {
  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;