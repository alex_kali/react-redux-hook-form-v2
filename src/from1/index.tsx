import {useEffect} from "react";
import {ResetButton} from "src/components/buttons/resetButton";
import {SubmitButton} from "src/components/buttons/submitButton";
import {TextInput} from "src/components/inputs/textInputs/textInput";
import {TextInputWithoutValidation} from "src/components/inputs/textInputWithoutValidation";
import {FormTitle, InputTitle} from "src/components/titles";
import {Form} from "src/react-redux-hook-form-v2/src";
import {useForm} from "src/react-redux-hook-form-v2/src";
import {WrapperButtonStyled} from "src/styled";

export const From1 = ({onSuccess, onChange, useEffects}: {onSuccess?: (data: any) => void,onChange?: (data: any) => void, useEffects?: Array<{deps: Array<string>, callback: (data:any) => void}>}) => {
  const form = useForm({
    name: 'form1',
    form: {
      isAutoFocus: true,
      onSuccess: onSuccess,
      onChange: onChange,
      initialData: {
        field1: 'aaa',
        field4: 'fsdfs',
      },
    },
  })

  if(useEffects){
    for(let effect of useEffects){
      form.useEffect(effect.callback, effect.deps)
    }
  }

  
  return (
    <Form form={form}>
      <FormTitle>Form1</FormTitle>
      
      <InputTitle>Field1</InputTitle>
      <TextInput name={'field1'} isRequired min={5} max={20}/>
      
      <InputTitle>Field2</InputTitle>
      <TextInputWithoutValidation name={'field2'}/>
      
      <InputTitle>Field3</InputTitle>
      <TextInput name={'field3'} isRequired/>
      
      <InputTitle>Field4</InputTitle>
      <TextInput name={'field4'}/>
      
      <WrapperButtonStyled>
        <SubmitButton/>
        <ResetButton/>
      </WrapperButtonStyled>
    </Form>
  )
}