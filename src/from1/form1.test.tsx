import {fireEvent, render, screen} from '@testing-library/react';
import {Provider} from "react-redux";
import {From1} from "src/from1/index";
import {store} from "src/store/store";

const form = {
  form: () => screen.getByTestId('form'),
  field1: () => screen.getByTestId('field1') as HTMLInputElement,
  field2: () => screen.getByTestId('field2') as HTMLInputElement,
  field3: () => screen.getByTestId('field3') as HTMLInputElement,
  field4: () => screen.getByTestId('field4') as HTMLInputElement,
  toSubmit: () => fireEvent(screen.getByText('Submit') as HTMLButtonElement, new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
    }),
  ),
  toReset: () => fireEvent(screen.getByText('Reset') as HTMLButtonElement, new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
    }),
  ),
  store: {
    form: () => store.getState().formStore['form1'],
    field1: () => store.getState().formStore['form1'].fields['field1'],
    field2: () => store.getState().formStore['form1'].fields['field2'],
    field3: () => store.getState().formStore['form1'].fields['field3'],
    field4: () => store.getState().formStore['form1'].fields['field4'],
  }
}


describe('form1', () => {
  it('should be a render correct render', () => {
    render(<Provider store={store}><From1 /></Provider>);
  
    expect(form.form()).toBeTruthy()
    expect(form.field1()).toBeTruthy()
    expect(form.field2()).toBeTruthy()
    expect(form.field3()).toBeTruthy()
    expect(form.field4()).toBeTruthy()
  });
  
  it('should be a correct initial state', () => {
    render(<Provider store={store}><From1 /></Provider>);
  
    expect(form.store.field1().data).toBe('aaa')
    expect(form.store.field2().data).toBe('')
    expect(form.store.field3().data).toBe('')
    expect(form.store.field4().data).toBe('fsdfs')
  
    expect(form.field1().value).toBe('aaa')
    expect(form.field2().value).toBe('')
    expect(form.field3().value).toBe('')
    expect(form.field4().value).toBe('fsdfs')
  });
  
  it('should be a correct auto focus', () => {
    render(<Provider store={store}><From1 /></Provider>);
    
    expect(form.store.form().focusField).toBe('field1')
    expect(form.field1()).toHaveFocus()
  });
  
  it('should be a correct focus order', () => {
    render(<Provider store={store}><From1 /></Provider>);
    
    expect(form.store.form().focusOrder).toStrictEqual(['field1', 'field2', 'field3', 'field4'])
  });
  
  it('should be a change value', () => {
    render(<Provider store={store}><From1 /></Provider>);
  
    fireEvent.change(form.field1(), {target: {value: 'ds'}})
  
    expect(form.store.field1().data).toBe('ds')
    expect(form.field1().value).toBe('ds')
  });
  
  it('should be a correct isTouch', () => {
    render(<Provider store={store}><From1 /></Provider>);
    
    expect(form.store.field1().isTouched).toBe(false)
    
    fireEvent.change(form.field1(), {target: {value: 'ds'}})
    
    expect(form.store.field1().isTouched).toBe(true)
  });
  
  it('should be a correct message error', () => {
    render(<Provider store={store}><From1 /></Provider>);
    
    expect(form.store.field1().messageError).toBe('')
    expect(form.store.field1().isError).toBe(null)
    
    fireEvent.change(form.field1(), {target: {value: 'ds'}})
    
    expect(form.store.field1().messageError).toBe('The minimum field length 5. ')
    expect(form.store.field1().isError).toBe(true)
  
    fireEvent.change(form.field1(), {target: {value: 'fdgfdd'}})
  
    expect(form.store.field1().messageError).toBe('')
    expect(form.store.field1().isError).toBe(false)
  });
  
  it('should be a correct change focus', () => {
    const onSuccess = jest.fn()
    render(<Provider store={store}><From1 onSuccess={onSuccess}/></Provider>);
    
    fireEvent.change(form.field1(), {target: {value: 'fsdfsdf'}})
    form.field2().focus()
    
    expect(form.store.form().focusField).toBe('field2')
    expect(form.field2()).toHaveFocus()
  
    fireEvent.keyDown(form.field2(), {key: 'Enter', code: 'Enter', charCode: 13})
  
    expect(form.store.form().focusField).toBe('field3')
    expect(form.field3()).toHaveFocus()
  
    fireEvent.keyDown(form.field3(), {key: 'Enter', code: 'Enter', charCode: 13})
  
    expect(form.store.form().focusField).toBe('field3')
    expect(form.field3()).toHaveFocus()
    expect(form.store.field3().messageError).toBe('This field is required.')
    expect(form.store.field3().isError).toBe(true)
  
    fireEvent.change(form.field3(), {target: {value: 'value1'}})
    fireEvent.keyDown(form.field3(), {key: 'Enter', code: 'Enter', charCode: 13})
  
    expect(form.store.form().focusField).toBe('field4')
    expect(form.field4()).toHaveFocus()
  
    fireEvent.keyDown(form.field4(), {key: 'Enter', code: 'Enter', charCode: 13})
    
    expect(onSuccess).toBeCalled()
    expect(onSuccess.mock.calls.length).toBe(1)
    expect(onSuccess).toBeCalledWith({
      "field1": "fsdfsdf",
      "field2": "",
      "field3": "value1",
      "field4": "fsdfs",
    })
  });
  
  it('should be a correct on change callback', () => {
    const onChange = jest.fn()
    render(<Provider store={store}><From1 onChange={onChange}/></Provider>);
    expect(onChange.mock.calls.length).toBe(0)
  
    fireEvent.change(form.field1(), {target: {value: 'rrr'}})
    expect(onChange.mock.calls.length).toBe(1)
    expect(onChange).toBeCalledWith({
      "field1": "rrr",
      "field2": "",
      "field3": "",
      "field4": "fsdfs",
    })
  
    fireEvent.change(form.field4(), {target: {value: 'ddd'}})
    expect(onChange.mock.calls.length).toBe(2)
    expect(onChange).toBeCalledWith({
      "field1": "rrr",
      "field2": "",
      "field3": "",
      "field4": "ddd",
    })
  
    fireEvent.keyDown(form.field4(), {key: 'Enter', code: 'Enter', charCode: 13})
    expect(onChange.mock.calls.length).toBe(2)
    
    fireEvent.keyDown(form.field2(), {key: 'Enter', code: 'Enter', charCode: 13})
    expect(onChange.mock.calls.length).toBe(2)
  })
  
  it('should be a correct on success callback', () => {
    const onSuccess = jest.fn()
    render(<Provider store={store}><From1 onSuccess={onSuccess}/></Provider>);
  
    expect(form.store.form().countUntouchedFields).toBe(4)
    expect(form.store.form().countUnValidFields).toBe(0)
    
    expect(form.store.field1().isTouched).toBe(false)
    expect(form.store.field2().isTouched).toBe(false)
    expect(form.store.field3().isTouched).toBe(false)
    expect(form.store.field4().isTouched).toBe(false)
  
    expect(form.store.field1().isError).toBe(null)
    expect(form.store.field2().isError).toBe(null)
    expect(form.store.field3().isError).toBe(null)
    expect(form.store.field4().isError).toBe(null)
    
    form.toSubmit()
    
    expect(onSuccess.mock.calls.length).toBe(0)
  
    expect(form.store.form().countUntouchedFields).toBe(0)
    expect(form.store.form().countUnValidFields).toBe(2)
    
    expect(form.store.field1().isTouched).toBe(true)
    expect(form.store.field2().isTouched).toBe(true)
    expect(form.store.field3().isTouched).toBe(true)
    expect(form.store.field4().isTouched).toBe(true)
  
    expect(form.store.field1().isError).toBe(true)
    expect(form.store.field2().isError).toBe(false)
    expect(form.store.field3().isError).toBe(true)
    expect(form.store.field4().isError).toBe(false)
  
    fireEvent.change(form.field1(), {target: {value: 'rrrrrr'}})
    fireEvent.change(form.field3(), {target: {value: 'rr'}})
  
    expect(form.store.form().countUntouchedFields).toBe(0)
    expect(form.store.form().countUnValidFields).toBe(0)
  
    form.toSubmit()
    
    expect(onSuccess.mock.calls.length).toBe(1)
    expect(onSuccess).toBeCalledWith({
      "field1": "rrrrrr",
      "field2": "",
      "field3": "rr",
      "field4": "fsdfs",
    })
  })
  
  it('should be a correct reset', async () => {
    render(<Provider store={store}><From1/></Provider>);
    
    await new Promise(resolve => setTimeout(resolve, 0))
  
    expect(form.store.form().resetData.countUntouchedFields).toBe(4)
    expect(form.store.form().resetData.countUnValidFields).toBe(0)
  
    expect(form.store.form().resetData.fields['field1'].isTouched).toBe(false)
    expect(form.store.form().resetData.fields['field2'].isTouched).toBe(false)
    expect(form.store.form().resetData.fields['field3'].isTouched).toBe(false)
    expect(form.store.form().resetData.fields['field4'].isTouched).toBe(false)
  
    expect(form.store.form().resetData.fields['field1'].isError).toBe(null)
    expect(form.store.form().resetData.fields['field2'].isError).toBe(null)
    expect(form.store.form().resetData.fields['field3'].isError).toBe(null)
    expect(form.store.form().resetData.fields['field4'].isError).toBe(null)
  
    expect(form.store.form().resetData.fields['field1'].data).toBe('aaa')
    expect(form.store.form().resetData.fields['field2'].data).toBe('')
    expect(form.store.form().resetData.fields['field3'].data).toBe('')
    expect(form.store.form().resetData.fields['field4'].data).toBe('fsdfs')
  
  
    fireEvent.change(form.field1(), {target: {value: '1'}})
    fireEvent.change(form.field1(), {target: {value: '2'}})
    fireEvent.change(form.field1(), {target: {value: '3'}})
    fireEvent.change(form.field1(), {target: {value: '4'}})
    
    form.toReset()
  
    expect(form.store.form().countUntouchedFields).toBe(4)
    expect(form.store.form().countUnValidFields).toBe(0)
  
    expect(form.store.form().fields['field1'].isTouched).toBe(false)
    expect(form.store.form().fields['field2'].isTouched).toBe(false)
    expect(form.store.form().fields['field3'].isTouched).toBe(false)
    expect(form.store.form().fields['field4'].isTouched).toBe(false)
  
    expect(form.store.form().fields['field1'].isError).toBe(null)
    expect(form.store.form().fields['field2'].isError).toBe(null)
    expect(form.store.form().fields['field3'].isError).toBe(null)
    expect(form.store.form().fields['field4'].isError).toBe(null)
  
    expect(form.store.form().fields['field1'].data).toBe('aaa')
    expect(form.store.form().fields['field2'].data).toBe('')
    expect(form.store.form().fields['field3'].data).toBe('')
    expect(form.store.form().fields['field4'].data).toBe('fsdfs')
  })

  it('should be a correct useEffect callback', () => {
    const useEffects:any = [
      {
        deps: ['field1','field2'], callback: ({field1, field2}:any) => {
          if(field2.isError !== null){
            if(field1.data !== field2.data){
              field2.isError = true
            }else{
              field2.isError = false
            }
          }
        }},
    ]
    render(<Provider store={store}><From1 useEffects={useEffects}/></Provider>);

    fireEvent.change(form.field1(), {target: {value: 'fsd'}})

    expect(form.store.form().fields['field1'].isError).toBe(true)
    expect(form.store.form().fields['field2'].isError).toBe(null)

    expect(form.store.form().countUntouchedFields).toBe(3)
    expect(form.store.form().countUnValidFields).toBe(1)

    fireEvent.change(form.field2(), {target: {value: 'ff'}})

    expect(form.store.form().fields['field1'].isError).toBe(true)
    expect(form.store.form().fields['field2'].isError).toBe(true)

    expect(form.store.form().countUntouchedFields).toBe(2)
    expect(form.store.form().countUnValidFields).toBe(2)

    fireEvent.change(form.field1(), {target: {value: 'fsdgfdsgd'}})

    expect(form.store.form().fields['field1'].isError).toBe(false)
    expect(form.store.form().fields['field2'].isError).toBe(true)

    expect(form.store.form().countUntouchedFields).toBe(2)
    expect(form.store.form().countUnValidFields).toBe(1)

    fireEvent.change(form.field2(), {target: {value: 'fsdgfdsgd'}})

    expect(form.store.form().fields['field1'].isError).toBe(false)
    expect(form.store.form().fields['field2'].isError).toBe(false)

    expect(form.store.form().countUntouchedFields).toBe(2)
    expect(form.store.form().countUnValidFields).toBe(0)
  })
  
});