export const required = function(this: any, value: boolean, messageError?:string ) {
  if(value && this.isNeedValidate) {
    if (!this.data) {
      this.isError = true
      this.messageError +=  messageError || `This field is required.`
      this.isNeedValidate = false
    }
  }
  return this
}