export const stringMax = function(this: any, value?: number, messageError?:string ) {
  if(this.isNeedValidate && value) {
    if (this.data.length > value) {
      this.isError = true
      this.messageError += messageError || `Максимальная длинна поля ${value}. `
    }
  }
  return this
}