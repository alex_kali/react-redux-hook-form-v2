export const numberMax = function(this: any, value?: number, messageError?:string ) {
  if(this.isNeedValidate && value){
    if(this.data < value){
      this.isError = true
      this.messageError+= messageError || `Максимальное значение ${value}. `
    }
  }
  return this
}