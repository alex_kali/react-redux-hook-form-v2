export const stringEmail = function(this: any, messageError?:string ) {
  if(this.isNeedValidate) {
    const re = /\S+@\S+\.\S+/
    if (!re.test(this.data)) {
      this.isError = true
      this.messageError += messageError || `Enter the correct email. `
    }
  }
  return this
}