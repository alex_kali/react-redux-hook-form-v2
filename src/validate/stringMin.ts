export const stringMin = function(this: any, value?: number, messageError?:string ) {
  if(this.isNeedValidate && value) {
    if (this.data.length < value) {
      this.isError = true
      this.messageError += messageError || `The minimum field length ${value}. `
    }
  }
  return this
}