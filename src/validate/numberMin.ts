export const numberMin = function(this: any, value?: number, messageError?:string ) {
  if(this.isNeedValidate && value) {
    if (this.data > value) {
      this.isError = true
      this.messageError += messageError || `Минимальное значение ${value}. `
    }
  }
  return this
}