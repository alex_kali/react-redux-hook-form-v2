import React from 'react';
import './App.css';
import {From1} from "src/from1";
import {store} from "src/store/store";
import {Provider} from "react-redux";
import {WrapperForm} from "src/styled";

function App() {
  const useEffects:any = [
    {
      deps: ['field1','field2'],
      callback: ({field1, field2}:any) => {
        console.log(field2.isError)
        if(field2.isError !== null){
          if(field1.data !== field2.data){
            field2.isError = true
          }else{
            field2.isError = false
          }
        }
      }},
  ]
  return (
    <div >
      <Provider store={store}>
        <WrapperForm>
          <From1 useEffects={useEffects} onSuccess={(data) => console.log('onFinish', data)}/>
        </WrapperForm>
      </Provider>
    </div>
  );
}

export default App;
