import styled from "styled-components";

export const ResetButtonStyled = styled.button`
  padding: 8px 16px;
  border: 1px solid rgba(0,0,0,0.3);
  font-size: 16px;
  border-radius: 8px;
`