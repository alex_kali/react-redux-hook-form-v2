import {ResetButtonStyled} from "src/components/buttons/resetButton/styled";
import {useResetButton} from "src/react-redux-hook-form-v2/src";

export const ResetButton = () => {
  const { toReset } = useResetButton()
  
  return (
    <>
      <ResetButtonStyled type={'button'} onClick={() => toReset()}>
        Reset
      </ResetButtonStyled>
    </>
  )
}