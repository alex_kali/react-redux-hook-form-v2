import {SubmitButtonStyled} from "src/components/buttons/submitButton/styled";
import {useButton} from "src/react-redux-hook-form-v2/src";

export const SubmitButton = () => {
  const { onSuccess, isAllFieldsTouched, isAllFieldsValidate } = useButton()
  
  return (
    <SubmitButtonStyled type={'button'} onClick={() => onSuccess()}>
      Submit
    </SubmitButtonStyled>
  )
}