import styled from "styled-components";

export const InputTitle = styled.div`
  margin-left: 2px;
  margin-top: 16px;
  margin-bottom: 8px;
  font-size: 18px;
`