import styled from "styled-components";

export const FormTitle = styled.div`
  font-size: 28px;
  margin-bottom: 32px;
  text-align: center;
`