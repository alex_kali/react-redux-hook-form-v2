import {FC} from "react";
import {DefaultInput} from "src/components/inputs/textInputs/defaultInput";
import {validate} from "src/validate";

interface ITextInput {
  name: string;
  isRequired?: boolean,
  min?: number,
  max?: number,
}

export const TextInput:FC<ITextInput> = ({name,isRequired, min, max}) => {
  return (
    <DefaultInput
      name={name}
      isRequired={isRequired}
      validate={(value) => validate(value).string().require(isRequired).min(min).max(max)}
      field={{
        data: ''
      }}
    />
  )
}