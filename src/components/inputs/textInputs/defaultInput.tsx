import {FC} from "react";
import {MessageErrorStyled, TextInputStyled, WrapperTextInputStyled} from "src/components/inputs/textInputs/styled";
import {useField} from "src/react-redux-hook-form-v2/src";
import {IUseField} from "src/react-redux-hook-form-v2/src/field/useField";

interface IDefaultInput extends IUseField<string>{
  isRequired?: boolean,
}

export const DefaultInput:FC<IDefaultInput> = ({name, field, validate, isRequired}) => {
  
  const {useData, useMessageError, useIsError, useIsTouched, useFocus} = useField({
    name,
    validate,
    field
  })
  
  const [data, setData] = useData()
  const [messageError, ] = useMessageError()
  const [isError, ] = useIsError()
  const [isTouched, setIsTouch] = useIsTouched()
  
  const {isFocus, toNext, toFocus, toUnFocus} = useFocus((isFocus) => {
    if(isFocus){
      document.getElementById(name)?.focus()
    }else{
      document.getElementById(name)?.blur()
    }
  })
  
  const handleKeyDown = (event:any) => {
    if (event.key === 'Enter' && !isError) {
      if(isTouched || !isRequired){
        toNext()
      }else{
        setIsTouch(true)
      }
    }
  }
  
  return (
    <>
      <WrapperTextInputStyled htmlFor={name} isFocus={isFocus} isError={isError} isTouched={isTouched}>
        <TextInputStyled
          isFocus={isFocus}
          isError={isError}
          type="text"
          value={data}
          onChange={(e) => setData(e.target.value)}
          onKeyDown={handleKeyDown}
          onFocus={toFocus}
          onBlur={toUnFocus}
          id={name}
          data-testid={name}
        />
      </WrapperTextInputStyled>
      <MessageErrorStyled isFocus={isFocus}>
        {messageError}
      </MessageErrorStyled>
   </>
)
}