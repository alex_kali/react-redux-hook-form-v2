import styled from "styled-components";

export const WrapperTextInputStyled = styled.label<{isFocus: boolean, isError: boolean, isTouched: boolean}>`
  display: block;
  padding: 8px 16px;
  border: 1px solid rgba(0,0,0,0.3);
  border-radius: 8px;
  transition-duration: 0.3s;
  transition-delay: 0s;
  &:hover {
    border-color: #57a8e9;
  }
  ${({isTouched}) => !isTouched ? 'opacity: 0.5;' : ''}
  ${({isError, isFocus}) => isError && !isFocus ? 'border-color: #ff3333 !important;' : ''}
  ${({isFocus}) => isFocus ? 'border-color: #57a8e9;box-shadow: 0 0 0 2px rgba(87,168,233, .2);opacity: 1;' : ''}
`

export const TextInputStyled = styled.input<{isFocus: boolean, isError: boolean}>`
  font-size: 20px;
  transition-duration: 0.3s;
  ${({isError, isFocus}) => isError && !isFocus ? 'color: #ff3333;' : ''}
`

export const MessageErrorStyled = styled.div<{isFocus: boolean}>`
  font-size: 12px;
  margin-top: 4px;
  margin-left: 4px;
  transition-duration: 0.3s;
  ${({isFocus}) => isFocus ? 'color: black;' : 'color: #ff3333;'}
`