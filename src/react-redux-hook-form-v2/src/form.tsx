import React, {FC} from "react";

interface IForm {
  children: React.ReactNode;
  form: any;
}

export const NameFormContext = React.createContext('');
export const Form:FC<IForm> = ({children, form}) => {
  return (
    <form data-testid={'form'}>
      <NameFormContext.Provider value={form.name}>
        {children}
      </NameFormContext.Provider>
    </form>
  )
}