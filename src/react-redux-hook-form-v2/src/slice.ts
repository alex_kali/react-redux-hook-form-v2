import {createSlice, current} from "@reduxjs/toolkit";
import {getFormData} from "./form/utils/getFormData";
export const initialState: any = {

};

const validate = (state:any, formName: string, fieldName:string) => {
  console.log(state[formName].fields[fieldName].isError)
  if(state[formName].fields[fieldName].validate){
    const {isError, messageError} = state[formName].fields[fieldName].validate(state[formName].fields[fieldName].data)
    if(state[formName].fields[fieldName].isError !== isError){
      if(isError){
        state[formName].countUnValidFields += 1
      }else if(state[formName].fields[fieldName].isError !== null){
        state[formName].countUnValidFields -= 1
      }
      state[formName].fields[fieldName].isError = isError
    }
    state[formName].fields[fieldName].messageError = messageError
  }else if (state[formName].fields[fieldName].isError === null) {
    state[formName].fields[fieldName].isError = false
  }
}

const finish = (state:any, action:any) => {
  if (state[action.payload.formName].countUntouchedFields) {
    const form = state[action.payload.formName]
    for (let field in form.fields) {
      if (!form.fields[field].isTouched) {
        form.fields[field].isTouched = true
        form.countUntouchedFields -= 1
        validate(state, action.payload.formName, field)
      }
    }
  }
  if (!state[action.payload.formName].countUnValidFields) {
    state[action.payload.formName].onSuccess(getFormData(state[action.payload.formName].fields))
    
  }
}

export const formSlice = createSlice({
  name: 'form-store',
  initialState,
  reducers: {
    createForm: (state, action) => {
      state[action.payload.formName] = action.payload.form
      if(action.payload.form.focusOrder.length){
        state[action.payload.formName].isDisableAutoPushFocusOrder = !!action.payload.form.focusOrder
        if(state[action.payload.formName].isAutoFocus){
          state[action.payload.formName].focusField = action.payload.form.focusOrder[0]
        }
      }
    },
    createField: (state, action) => {
      state[action.payload.formName].fields[action.payload.fieldName] = action.payload.field
      
      if(state[action.payload.formName].initialData?.[action.payload.fieldName]){
        state[action.payload.formName].fields[action.payload.fieldName].data = state[action.payload.formName].initialData[action.payload.fieldName]
      }
      
      if(action.payload.field.isTouched){
        validate(state, action.payload.formName, action.payload.fieldName)
      }else{
        state[action.payload.formName].countUntouchedFields += 1
      }
    },
    setData: (state, action) => {
      const field = state[action.payload.formName].fields[action.payload.fieldName]
      field.data = action.payload.data
      
      if(!field.isTouched){
        field.isTouched = true
        state[action.payload.formName].countUntouchedFields -= 1
      }
  
      validate(state, action.payload.formName, action.payload.fieldName)
      
      if(state[action.payload.formName].onChange){
        state[action.payload.formName].onChange(getFormData(state[action.payload.formName].fields))
      }

      console.log(state[action.payload.formName].fields[action.payload.fieldName].isError)
      if(state[action.payload.formName].allUseEffectDeps.includes(action.payload.fieldName)){
        for(let effect of state[action.payload.formName].useEffects){
          if(effect.deps.includes(action.payload.fieldName)){
            let fields:any = {}
            
            for(let i of effect.deps){
              fields[i] = {...state[action.payload.formName].fields[i]}
            }

            effect.callback(fields)

            for(let i of effect.deps){
              const data = fields[i]
              if(data.isError !== undefined && (data.isError !== state[action.payload.formName].fields[i].isError)){
                if(data.isError){
                  state[action.payload.formName].countUnValidFields += 1
                }else if(!(data.isError === null && state[action.payload.formName].fields[i].isError === false) && state[action.payload.formName].fields[i].isError !== null){
                  state[action.payload.formName].countUnValidFields -= 1
                }
              }

              if(data.isTouched !== undefined && data.isTouched !== state[action.payload.formName].fields[i].isTouched){
                if(data.isTouched){
                  state[action.payload.formName].countUntouchedFields -= 1
                }else{
                  state[action.payload.formName].countUntouchedFields += 1
                }
              }

              state[action.payload.formName].fields[i] = data
            }
          }
        }
      }
    },
    setIsError: (state, action) => {
      state[action.payload.formName].fields[action.payload.fieldName].isError = action.payload.isError
    },
    setMessageError: (state, action) => {
      state[action.payload.formName].fields[action.payload.fieldName].messageError = action.payload.messageError
    },
    setIsTouch: (state, action) => {
      const field = state[action.payload.formName].fields[action.payload.fieldName]
      
      if(field.isTouched !== action.payload.isTouched){
        if(action.payload.isTouched){
          validate(state, action.payload.formName, action.payload.fieldName)
        }else{
          field.messageError = ''
          field.isError = null
        }
        field.isTouched = action.payload.isTouched
      }
    },
    pushFocusOrder: (state, action) => {
      if(state[action.payload.formName].focusOrder.length === 0 && state[action.payload.formName].isAutoFocus){
        state[action.payload.formName].focusField = action.payload.fieldName
      }
      state[action.payload.formName].focusOrder.push(action.payload.fieldName)
    },
    toNextFocus: (state, action) => {
      const form = state[action.payload.formName]
      const index = form.focusOrder.indexOf(action.payload.fieldName)
      
      if(index === form.focusOrder.length - 1){
        form.focusField = null
        finish(state, action)
      }else{
        form.focusField = form.focusOrder[index + 1]
      }
    },
    toFocus: (state, action) => {
      state[action.payload.formName].focusField = action.payload.fieldName
    },
    toUnFocus: (state, action) => {
      state[action.payload.formName].focusField = null
    },
    toFinish: (state, action) => {
      finish(state, action)
    },
    saveResetData: (state, action) => {
      state[action.payload.formName].resetData = {
        countUnValidFields: state[action.payload.formName].countUnValidFields,
        countUntouchedFields: state[action.payload.formName].countUntouchedFields,
        fields: state[action.payload.formName].fields,
      }
    },
    toResetForm: (state, action) => {
      state[action.payload.formName].countUnValidFields = state[action.payload.formName].resetData.countUnValidFields
      state[action.payload.formName].countUntouchedFields = state[action.payload.formName].resetData.countUntouchedFields
      state[action.payload.formName].fields = state[action.payload.formName].resetData.fields
    },
    addUseEffect: (state, action) => {
      state[action.payload.formName].useEffects.push({
        callback: action.payload.callback,
        deps: action.payload.deps,
      })
  
      state[action.payload.formName].allUseEffectDeps = Array.from(new Set([...state[action.payload.formName].allUseEffectDeps, ...action.payload.deps]))
    },
  },
});

export const { createForm, createField, setData, setIsError, setMessageError, setIsTouch, pushFocusOrder, toNextFocus, toFocus, toUnFocus, toFinish, saveResetData, toResetForm, addUseEffect } = formSlice.actions

export const formReducer =  formSlice.reducer;