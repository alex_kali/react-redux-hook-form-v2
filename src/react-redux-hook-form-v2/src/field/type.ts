export interface IField<T> {
  data: T;
  isError: boolean | null;
  messageError: string;
  isTouched: boolean;
}