import {useContext, useEffect, useMemo, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {initialField} from "./initial";
import {IField} from "./type";
import {NameFormContext} from "../form";
import {
  createField,
  pushFocusOrder,
  setData,
  setIsError,
  setIsTouch,
  setMessageError, toFocus, toNextFocus, toUnFocus
} from "../slice";

export interface IUseField<T> {
  name: string;
  field?: Partial<IField<T>>;
  validate?: (data: T) => {isError: boolean, messageError?: string}
}


export const useField = <Type>({name, field, validate}: IUseField<Type>, ) => {
  const dispatch = useDispatch()
  const formName = useContext(NameFormContext);
  const isDisableAutoPushFocusOrder = useSelector((state:any) => state.formStore[formName].isDisableAutoPushFocusOrder)
  
  useMemo(()=>{
    dispatch(createField({
      fieldName: name,
      formName: formName,
      field: { validate, ...initialField, ...field }
    }))
  }, [])
  
  return {
    useData: () => [
      useSelector((state:any) => state.formStore[formName].fields[name].data),
      (data: Type) => dispatch(setData({formName,fieldName: name, data: data}))
    ] as [Type, (data: Type) => void],
    
    useIsError: () => [
      useSelector((state:any) => state.formStore[formName].fields[name].isError),
      (isError: boolean) => dispatch(setIsError({formName,fieldName: name, isError: isError}))
    ] as [boolean, (data: boolean) => void],
    
    useMessageError: () => [
      useSelector((state:any) => state.formStore[formName].fields[name].messageError),
      (messageError: string) => dispatch(setMessageError({formName,fieldName: name, messageError: messageError}))
    ] as [string, (data: string) => void],
  
    useIsTouched: () => [
      useSelector((state:any) => state.formStore[formName].fields[name].isTouched),
      (isTouched: boolean) => dispatch(setIsTouch({formName,fieldName: name, isTouched: isTouched}))
    ] as [boolean, (data: boolean) => void],
    
    useFocus: (onChangeFocus?: (isFocus: boolean) => void) => {
      const isFocus = useSelector((state:any) => state.formStore[formName].focusField === name)
      
      useMemo(()=>{
        if(!isDisableAutoPushFocusOrder){
          dispatch(pushFocusOrder({formName,fieldName: name}))
        }
      }, [])
      
      const isNeedToCallOnChangeFocus = useRef(false);
      
      if(onChangeFocus){
        
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEffect(() => {
          if(isNeedToCallOnChangeFocus.current){
            onChangeFocus(isFocus)
          }else{
            isNeedToCallOnChangeFocus.current = true
            if(isFocus){
              onChangeFocus(isFocus)
            }
          }
        }, [isFocus])
      }
      
      return {
        isFocus: isFocus,
        toNext: () => {
          dispatch(toNextFocus({formName, dispatch, fieldName: name}))
        },
        toFocus: () => {
          if(!isFocus){
            dispatch(toFocus({formName, fieldName: name}))
          }
        },
        toUnFocus: () => {
          if(isFocus){
            dispatch(toUnFocus({formName}))
          }
        },
      }
    }
  }
}