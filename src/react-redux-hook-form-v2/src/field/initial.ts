import {IField} from "./type";


export const initialField: IField<null> = {
  data: null,
  isError: null,
  isTouched: false,
  messageError: '',
}