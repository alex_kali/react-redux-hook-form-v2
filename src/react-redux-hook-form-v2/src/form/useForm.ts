import {useMemo} from "react";
import {useDispatch} from "react-redux";
import {IField} from "../field/type";

import {initialForm} from "./initial";
import {ICreateForm} from "./type";
import {addUseEffect, createForm, saveResetData} from "../slice";

interface IUseForm {
  name: string;
  form?: ICreateForm;
}

interface IFieldCustom<T> extends IField<T>{
  validate: (data: T) => {isError: boolean, messageError?: string}
  setData: (data: Partial<IField<T>>) => void
}


export type IUseEffect = <T extends string>(
  callback: (data: Record<T, IFieldCustom<any>>) => void,
  deps: Array<T>
) => void

export const useForm = ({name, form}: IUseForm) => {
  const dispatch = useDispatch()
  
  useMemo(()=>{
    dispatch(createForm({formName: name, form: {...initialForm, ...form}}))
    setTimeout(() => {
      dispatch(saveResetData({formName: name}))
    })
  }, [])
  
  const useEffect:IUseEffect = (callback, deps) => {
    dispatch(addUseEffect({callback: callback, deps: deps, formName: name}))
  }
  
  return {name: name, useEffect: useEffect}
}