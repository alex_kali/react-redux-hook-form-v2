import {IForm} from "./type";


export const initialForm:IForm = {
  focusOrder: [],
  isAutoFocus: false,
  fields: {},
  countUntouchedFields: 0,
  countUnValidFields: 0,
  useEffects: [],
  allUseEffectDeps: [],
}