import {useContext} from "react";
import {useDispatch, useSelector} from "react-redux";
import {NameFormContext} from "../../form";
import {toFinish} from "../../slice";

export const useButton = () => {
  const dispatch = useDispatch()
  const formName = useContext(NameFormContext);
  return {
    onSuccess: () => dispatch(toFinish({formName})),
    isAllFieldsTouched: useSelector((state:any) => !state.formStore[formName].countUntouchedFields),
    isAllFieldsValidate: useSelector((state:any) => !state.formStore[formName].countUnValidFields),
  }
}