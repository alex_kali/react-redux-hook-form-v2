import {useContext} from "react";
import {useDispatch} from "react-redux";
import {NameFormContext} from "../../form";
import {saveResetData, toResetForm} from "../../slice";

export const useResetButton = () => {
  const dispatch = useDispatch()
  const formName = useContext(NameFormContext);
  return {
    saveResetData: () => dispatch(saveResetData({formName})),
    toReset: () => dispatch(toResetForm({formName})),
  }
}