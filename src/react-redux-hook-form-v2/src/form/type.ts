export interface IForm {
  focusOrder: Array<string>,
  isAutoFocus: boolean,
  onSuccess?: (data: any) => void,
  fields: {},
  countUntouchedFields: number,
  countUnValidFields: number,
  initialData?: any;
  useEffects: [],
  allUseEffectDeps: [],
}

export interface ICreateForm {
  focusOrder?: Array<string>,
  isAutoFocus?: boolean,
  onSuccess?: (data: any) => void,
  initialData?: any,
  onChange?: (data: any) => void,
}