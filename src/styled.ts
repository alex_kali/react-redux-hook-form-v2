import styled from "styled-components";

export const WrapperForm = styled.div`
  border: 1px solid rgba(0,0,0,0.3);
  padding: 48px;
  min-height: calc(100vh - 60px);
  width: 700px;
  margin: auto;
  margin-top: 30px;
  border-radius: 16px;
`

export const WrapperButtonStyled = styled.div`
  margin-top: 32px;
  display: flex;
  justify-content: space-between;
`